<?php
/**
 * Template Name: Instructors
 **/

get_header(); ?>

<div id="content-area" class="instructors-content">
	<div class="container clearfix">
		<div id="head-area" class="instructors-head">
			<div class="instructors-head-photo">
				<img src="/wp-content/uploads/2016/04/BB.jpeg" alt="Brian Brodersen">
			</div>
			<h2>Brian Brodersen</h2>
			<h3>President</h3>
			<p>Brian has been involved in pastoral ministry for over 30 years serving as senior pastor of Calvary Chapel Vista in Vista, California, Calvary Chapel Westminster in London, England, and now <a href="http://cccm.com" target="_blank">Calvary Chapel Costa Mesa</a>. He grew up in Southern California. After becoming a Christian at age 22, he began attending Calvary Chapel Costa Mesa where he met his wife, Cheryl, the youngest daughter of senior pastor Chuck Smith. They were married in 1980, and have four grown children, four grandsons, and one granddaughter. Pastor Brian’s radio program, <a href="http://backtobasicsradio.com" target="_blank">Back to Basics</a>, can be heard daily on <a href="http://kwve.com" target="_blank">KWVE</a>.</p>
		</div>
		<div id="head-area" class="instructors-head">
			<div class="instructors-head-photo">
				<img src="/wp-content/uploads/2016/04/john-hwang.jpg" alt="John Hwang">
			</div>
			<h2>John Hwang</h2>
			<h3>Director</h3>
			<p>John has been serving King Jesus for more than 20 years as a pastor, missionary, and worship leader in three different continents: North America, South America, and Europe. The call to ministry came to him when he was 14 years old, and since then, God has opened doors for him to teach His word in churches, conferences, camps, and colleges. His preaching style is expositional with a strong emphasis of Christ in the Biblical text. Today, he is an assistant pastor at Calvary Chapel Costa Mesa and the director of the Pastors School.</p>
		</div>
	</div>
	<div class="container clearfix">
		<div id="main-area" class="instructors-main">

<?php
	$args = array(
		'post_type' => 'instructors',
		'posts_per_page' => -1,
		'orderby' => 'ID',
		'order' => 'ASC'
		);
	$query = new WP_Query( $args );
	while ( $query->have_posts() ) : $query->the_post();
?>
	<?php if (et_get_option('vertex_integration_single_top') <> '' && et_get_option('vertex_integrate_singletop_enable') == 'on') echo(et_get_option('vertex_integration_single_top')); ?>

	<article class="entry clearfix instructors">
		<div class="instructors-img">
			<?php if ( has_post_thumbnail() ) :
					the_post_thumbnail();
				else : ?>
					<img src="/wp-content/themes/pastors-school-theme/img/placeholder.jpg" />
			<?php endif; ?>
		</div>
		<div class="instructors-text">
			<h2><?php the_title(); ?> </h2>
			<?php
			the_content();
			?>
		</div>
		<?php

		wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Vertex' ), 'after' => '</div>' ) );
	?>
	<?php
		if ( et_get_option('vertex_468_enable') == 'on' ){
			if ( et_get_option('vertex_468_adsense') <> '' ) echo( et_get_option('vertex_468_adsense') );
			else { ?>
				<a href="<?php echo esc_url(et_get_option('vertex_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('vertex_468_image')); ?>" alt="468 ad" class="foursixeight" /></a>
	<?php 	}
		}
	?>
	</article> <!-- .entry -->

	<?php if (et_get_option('vertex_integration_single_bottom') <> '' && et_get_option('vertex_integrate_singlebottom_enable') == 'on') echo(et_get_option('vertex_integration_single_bottom')); ?>

	<?php
		if ( comments_open() && 'on' == et_get_option( 'vertex_show_postcomments', 'on' ) )
			comments_template( '', true );
	?>
<?php endwhile; ?>

		</div> <!-- #main-area -->
		<!-- No sidebar enabled -->
	</div> <!-- .container -->
</div> <!-- #content-area -->

<?php get_footer(); ?>
