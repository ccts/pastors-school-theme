<?php
/**
 * Template Name: Classes
 **/

get_header(); ?>

<div id="content-area" class="classes-content">
	<div class="container clearfix">
		<div id="main-area" class="classes">

<?php
	$args = array(
		'post_type' => 'classes',
		'posts_per_page' => -1,
		'orderby' => 'ID',
		'order' => 'ASC'
		);
	$query = new WP_Query( $args );
	while ( $query->have_posts() ) : $query->the_post();
?>
	<?php if (et_get_option('vertex_integration_single_top') <> '' && et_get_option('vertex_integrate_singletop_enable') == 'on') echo(et_get_option('vertex_integration_single_top')); ?>

	<article class="entry clearfix classes">

		<div class="classes-text">
			<h2><?php the_title(); ?> </h2>
			<?php
			the_content();
			?>
		</div>
		<?php

		wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'Vertex' ), 'after' => '</div>' ) );
	?>
	<?php
		if ( et_get_option('vertex_468_enable') == 'on' ){
			if ( et_get_option('vertex_468_adsense') <> '' ) echo( et_get_option('vertex_468_adsense') );
			else { ?>
				<a href="<?php echo esc_url(et_get_option('vertex_468_url')); ?>"><img src="<?php echo esc_attr(et_get_option('vertex_468_image')); ?>" alt="468 ad" class="foursixeight" /></a>
	<?php 	}
		}
	?>
	</article> <!-- .entry -->

	<?php if (et_get_option('vertex_integration_single_bottom') <> '' && et_get_option('vertex_integrate_singlebottom_enable') == 'on') echo(et_get_option('vertex_integration_single_bottom')); ?>

	<?php
		if ( comments_open() && 'on' == et_get_option( 'vertex_show_postcomments', 'on' ) )
			comments_template( '', true );
	?>
<?php endwhile; ?>

		</div> <!-- #main-area -->
		<!-- No sidebar enabled -->
	</div> <!-- .container -->
</div> <!-- #content-area -->

<?php get_footer(); ?>
