<?php if ( is_home() ) : ?>
	<div id="pre-footer">
		<div class="container">
			<p class="tagline"><?php bloginfo( 'description' ); ?></p>

			<br />

			<?php et_vertex_action_button(); ?>
		</div> <!-- .container -->
	</div> <!-- #pre-footer -->
<?php endif; ?>

	<footer id="main-footer">
		<div class="container">
			<?php get_sidebar( 'footer' ); ?>

			<p id="footer-info">2016 <a href="http://cts.cccm.com">Calvary Technical Services</a> | <a href="http://cccm.com">Calvary Chapel Costa Mesa</a> | <a href="cccmps.com">Pastors School</a> | <a href="https://facebook.com/ThePastorsSchool/"><?php echo file_get_contents('wp-content/themes/pastors-school-theme/img/06-facebook.svg' ); ?></a><a href="https://twitter.com/cccmps"><?php echo file_get_contents('wp-content/themes/pastors-school-theme/img/03-twitter.svg' ); ?></a><a href="https://instagram.com/cccmps/"><?php echo file_get_contents('wp-content/themes/pastors-school-theme/img/38-instagram.svg'); ?></a></p>
		</div> <!-- .container -->
	</footer> <!-- #main-footer -->

	<?php wp_footer(); ?>
</body>
</html>