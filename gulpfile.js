// Gulp
var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var svgmin = require('gulp-svgmin');

// Browser Sync
var browserSync = require('browser-sync').create();

// Sync Task
gulp.task('fullSync', function(){
	browserSync.init({
		server: {
                        proxy: '192.168.33.10'
                },	
                });
	gulp.watch('src/css/*.scss', ['sass']);
	gulp.watch('src/js/*.js', ['uglify']);
	gulp.watch('src/img/*.svg', ['svgmin']);
	gulp.watch('app/*.php').on('change', browserSync.reload);

});

// Gulp Tasks
gulp.task('sass', function(){
        return gulp.src('src/css/*.scss')
        .pipe(sass({outputStyle: "compressed"}).on('error', sass.logError))
        .pipe(gulp.dest('./'))
        .pipe(browserSync.stream())
});

gulp.task('uglify', function(){
        return gulp.src('src/js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
        .pipe(browserSync.stream())
});

gulp.task('svgmin', function () {
	return gulp.src('src/img/*.svg')
		.pipe(svgmin())
		.pipe(gulp.dest('./img'));
});
