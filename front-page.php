<?php
if ( is_front_page() && is_page() ) {
	include( get_page_template() );
	return;
}

if ( 'on' === et_get_option( 'vertex_blog_style', 'false' ) ) {
	get_template_part( 'index' );
	return;
}
?>

<?php get_header(); ?>

<?php if ( 'on' === et_get_option( 'vertex_featured', 'on' ) ) { ?>
<section class="home-block et-even et-slider-area pastors-theme">
	<div class="container">
		<div class="directors-welcome">
				<h1>Welcome from the Director</h1>
				<br>
				<p>Calvary Chapel has a rich history of planting churches that preach the Gospel and teach God's word. Since Calvary Chapel first started in 1965, God has expanded our numbers from 1 to over 1,700 churches, and He has expanded our reach from Southern California to many different countries in 6 different continents.</p>

				<p>We praise God for what He has done, but we believe He wants to do more today. There is still more people who need to hear the Gospel and to be discipled in their relationship with Jesus through a simple and clear teaching of the Word of God. This is why the Pastors School exists.</p>

				<p>Our mission is to train men who are called by God to be pastors to plant Christ-centered, Bible-teaching churches, and to lead, nourish, and care for God's people like Jesus. We do this through Biblical instruction, pastoral discipleship, and practical application. If God has called you to be a pastor serving God’s people, then I invite you to pray about joining us at the Pastors School. You can learn about us and apply for our school on our website. We are excited to see God raise up and send out a new generation of church-planters and pastors obeying Jesus’ Great Commission in these last days (Mark 16:15, Matthew 28:18-20).</p>

				<p><i>“Now may the God of peace who brought up our Lord Jesus from the dead, that great Shepherd of the sheep, through the blood of the everlasting covenant, make you complete in every good work to do His will, working in you what is well pleasing in His sight, through Jesus Christ, to whom be glory forever and ever. Amen.” (Hebrews 13:20-21)</i></p>
			</p>
		</div>
		<div class="directors-video">
			<iframe src="https://player.vimeo.com/video/163433911" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		</div>
	</div>
	<div class="container">
		<div class="directors-application">
			<h1>Apply Now</h1>
			<p>Our school is designed for men whom God called to be pastors serving in pastoral ministry. Click Here to apply now!</p>
			<a href="/application" class="more">Application</a>
		</div>
	</div> <!-- .container -->
</section>
<?php } ?>

<?php get_footer(); ?>