<?php
// Calls in Vertex Theme's CSS File
// && calls in coffee.css for minification purposes
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/coffee.css' );
}

// Creates Classes Custom Post Type
function classes_init() {
    $args = array(
      'label' => 'Classes',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'classes'),
        'query_var' => true,
        'menu_icon' => 'dashicons-welcome-learn-more',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'revisions',
            'thumbnail',
            'page-attributes',)
        );
    register_post_type( 'classes', $args );
}
add_action( 'init', 'classes_init' );

// Creates Instructors Custom Post Type
function instructors_init() {
    $args = array(
      'label' => 'Instructors',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'instructors'),
        'query_var' => true,
        'menu_icon' => 'dashicons-id',
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'trackbacks',
            'custom-fields',
            'revisions',
            'thumbnail',
            'page-attributes',)
        );
    register_post_type( 'instructors', $args );
}
add_action( 'init', 'instructors_init' );

/*
 * Use this to set the default showing date in The Events Calendar
 */
function tribe_set_default_date () {
    if ( !is_single() && empty( $_REQUEST['tribe-bar-date'] ) ) {
        $today = date('Y-m-j');
        $_REQUEST['tribe-bar-date'] = $today;
    }
}
add_action( 'parse_query', 'tribe_set_default_date' );

/*
 * Adds gmaps API shortcode for optional inclusion on the Contact page
 */
//[gmapsembed]
function gmaps_func( $atts ){
    ob_start(); ?>
        <iframe width="600" height="450" frameborder="0" style="border:0"
        src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJOyY957LY3IARXeDbOuZMjpI&key=AIzaSyB7K2aTLDKRno7HviY2jQGDFpdwMM7eWAs" allowfullscreen></iframe>
    <?php $a = get_ob_contents();
}

function register_shortcodes() {
    add_shortcode( 'gmapsembed', 'gmaps_func' );
}
add_action( 'init', 'register_shortcodes');
?>
