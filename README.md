# Pastors School Theme
### A WordPress Child Theme for application with Elegant Themes' "Vertex" theme
Features:
- minification and concatination of js and scss files with gulp.js
- Addition of a loopable instructors and classes section
### To Install
Make sure your working/deployment environment has the following:
- node.js
- gulp.js with the following plugins
	- gulp-sass
	- gulp-uglify
	- browser-sync